# Global variables
# --------------------------------------
export PATH="$HOME/.zsh/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"
export PATH="/usr/local/bin:$PATH"
export PATH="./bin:$PATH"


# Go
export GOPATH="/usr/local/share/go"
export PATH="$PATH:$GOPATH/bin"
